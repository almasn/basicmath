package com.attracotr.demo.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

@Component
public class SimpleFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("Remote Host: " + request.getRemoteHost());
		System.out.println("Remote Addr: " + request.getRemoteAddr());
		
		HttpServletRequest wrequest = (HttpServletRequest) request;
		
		chain.doFilter(new HttpTextFilter(wrequest), response);
	}

}
