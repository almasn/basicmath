package com.attracotr.demo.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class HttpTextFilter extends HttpServletRequestWrapper {

	public HttpTextFilter(HttpServletRequest request) {
		super(request);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] originalValues = super.getParameterValues(name);
		
		for (int i=0;i<originalValues.length;i++)
			originalValues[i] = originalValues[i].replaceAll("1", "");
		
		return originalValues;
	}

	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		
		value = value.replaceAll("1", "");
		
		return value;
	}
	
}
