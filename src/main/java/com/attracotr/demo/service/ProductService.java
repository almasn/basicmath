package com.attracotr.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.attracotr.demo.model.Product;

@Service
public class ProductService implements ProductServiceInterface{

	private static Map<String, Product> productList = new HashMap<>();
	
	static{
		Product honey = new Product();
		honey.setId("1");
		honey.setName("Honey");

		Product almond = new Product();
		almond.setId("2");
		almond.setName("Almond");

		productList.put(honey.getId(), honey);
		productList.put(almond.getId(), almond);
	}

	@Override
	public Product getProduct(int id) {
		return productList.get(Integer.toString(id));
	}

	@Override
	public Map<String, Product> getAllProducts() {
		return productList;
	}

	@Override
	public boolean saveProduct(Product product) {

		productList.put(product.getId(), product);

		return true;
	}

	@Override
	public boolean deleteProduct(Product product) {
		productList.remove(product.getId());
		
		return true;
	}
}
