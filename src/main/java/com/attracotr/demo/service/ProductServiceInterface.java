package com.attracotr.demo.service;

import java.util.Map;

import com.attracotr.demo.model.Product;

public interface ProductServiceInterface {
	
	public Product getProduct(int id);

	public Map<String, Product> getAllProducts();
	
	public boolean saveProduct(Product product);

	public boolean deleteProduct(Product product);

}
