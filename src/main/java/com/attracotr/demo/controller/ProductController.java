package com.attracotr.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.attracotr.demo.exception.ProductNotFoundException;
import com.attracotr.demo.model.Product;
import com.attracotr.demo.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ResponseEntity<Object> getProduct() {
		return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> getProduct(@PathVariable("id") String id) {
		
		if (!productService.getAllProducts().containsKey(id))
			throw new ProductNotFoundException();

		return new ResponseEntity<>(productService.getAllProducts().get(id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
		
		if (productService.saveProduct(product))
			return new ResponseEntity<>(HttpStatus.CREATED);
		else
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deleteProduct(@RequestBody Product product) {
		
		if (productService.deleteProduct(product))
			return new ResponseEntity<>(HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
	}

	@RequestMapping(value = "/product/download", method = RequestMethod.GET)
	public ResponseEntity<Object> download(HttpServletResponse response) throws IOException {
		
		response.getOutputStream().write(Files.readAllBytes(new File("C:\\Users\\AlmasN\\Documents\\страховка.docx").toPath()));
		
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/product/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Object> upload(@RequestParam("file") MultipartFile f) throws IOException {
		
		FileOutputStream fw = new FileOutputStream(new File("C:\\Users\\AlmasN\\Desktop\\test.txt"));
		fw.write(f.getBytes());
		fw.close();
		
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<Object> addProduct(@RequestParam("id") String id,
			@RequestParam("tx") String tx,
			@RequestParam("param2") String param2) {
		
		System.out.println("id : " + id);
		System.out.println("tx : " + tx);
		System.out.println("param2 : " + param2);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
