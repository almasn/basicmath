package com.attracotr.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.attracotr.demo.exception.ProductNotFoundException;

@ControllerAdvice
public class ProductExceptionController {

	@ExceptionHandler(value = ProductNotFoundException.class)
	public ResponseEntity<Object> productNotFoundException(ProductNotFoundException exception) {
		return new ResponseEntity<>("Продукт не найден", HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = NullPointerException.class)
	public ResponseEntity<Object> allException(NullPointerException exception) {
		return new ResponseEntity<>("Неизвестная ошибка", HttpStatus.FORBIDDEN);
	}
}
