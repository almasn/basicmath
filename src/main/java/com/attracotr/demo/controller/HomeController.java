package com.attracotr.demo.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@RequestMapping("/sum/{a}/{b}")
	public int sum(@PathVariable("a") int a, @PathVariable("b") int b) {
		return a + b;
	}
	
	@RequestMapping("/minus/{a}/{b}")
	public int minus(@PathVariable("a") int a, @PathVariable("b") int b) {
		return a - b;
	}
	
	@RequestMapping("/multiply/{a}/{b}")
	public int multiply(@PathVariable("a") int a, @PathVariable("b") int b) {
		return a * b;
	}
	
	@RequestMapping("/divide/{a}/{b}")
	public double divide(@PathVariable("a") int a, @PathVariable("b") int b) {
		return (double)a / (double)b;
	}
	
	@RequestMapping("/sumplus1/{a}")
	public double sumplus1(@PathVariable("a") int a) {
		return a + 1;
	}
}
